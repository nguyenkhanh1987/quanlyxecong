<?php

namespace App\Http\Controllers;

use App\Journey;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class JourneyController extends Controller
{
    public function getDistance(Request $request) {
        $data = $request->all();
        
        $waypoints = urlencode("Thái Bình|Nam Định");

        $ggurl = "https://maps.googleapis.com/maps/api/directions/json?key=AIzaSyAQAGv-AmIr0j3rSKvKNtjnJ36FcK-w8C8&origin=".urlencode($data['origins']).'&destination='.urlencode($data['dest']).'&waypoints='.$waypoints;
        
        //$ggurl = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyAQAGv-AmIr0j3rSKvKNtjnJ36FcK-w8C8&origins=Thai+Nguyen&destinations=Nghe+An";
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $ggurl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        
        $response_a = json_decode($response);

        if($response_a->status=="OK") {
            $totalKm = 0;
            foreach ($response_a->routes[0]->legs as $key => $value) {
                $totalKm += $value->distance->value;
            }
            echo $totalKm/1000;
        }else{
            echo "0";
        }
        exit;
    }
    public function getGiaXang() {
        $petrolimex_url = "https://www.petrolimex.com.vn/";
        $content = file_get_contents($petrolimex_url);
        $content = explode('<div id="vie_p6_PortletContent">',$content);
        $content = explode('</div>',$content[1]);
        //$xang_95_v1 = strip_tags($content[4]);
        $xang_95_v2 = (double) strip_tags($content[5]);
        return $xang_95_v2;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Journey::paginate(20);
        return view('journeys.index',compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $giaxang = $this->getGiaXang();
        return view('journeys.create',compact('giaxang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Journey  $journey
     * @return \Illuminate\Http\Response
     */
    public function show(Journey $journey)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Journey  $journey
     * @return \Illuminate\Http\Response
     */
    public function edit(Journey $journey)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Journey  $journey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Journey $journey)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Journey  $journey
     * @return \Illuminate\Http\Response
     */
    public function destroy(Journey $journey)
    {
        //
    }
}
