<?php

namespace App\Http\Controllers;

use App\Driver;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Driver::paginate(20);
        return view('drivers.index',compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('drivers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => ['required'],
            'banglai' => ['required'],
            'ngaycapbang' => ['required'],
            'thoihangbanglai' => ['required']
        ]);
        if($request->has('published')) {
            $request->merge(['published'=>1]);
        }else{
            $request->merge(['published'=>0]);
        }
        $request->merge(['organization_id'=>1]);
        $item = new Driver($request->except('_token'));
        $item->save();
        return redirect()->route('driver.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function show(Driver $driver)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function edit(Driver $driver)
    {
        return view('drivers.edit',compact('driver'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Driver $driver)
    {
        $validatedData = $request->validate([
            'name' => ['required'],
            'banglai' => ['required'],
            'ngaycapbang' => ['required'],
            'thoihangbanglai' => ['required']
        ]);
        if($request->has('published')) {
            $request->merge(['published'=>1]);
        }else{
            $request->merge(['published'=>0]);
        }
        $request->merge(['organization_id'=>1]);
        $driver->update($request->except('_token'));
        return redirect()->route('driver.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function destroy(Driver $driver)
    {
        $driver->delete();
        return redirect()->route('driver.index')->with('info',"Xóa xong!");
    }
}
