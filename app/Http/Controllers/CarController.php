<?php

namespace App\Http\Controllers;

use App\Car;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Car::paginate(20);
        return view('cars.index',compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'bienso' => ['required', 'unique:cars', 'max:255'],
            'mucnhienlieu' => ['required'],
        ]);
        if($request->has('published')) {
            $request->merge(['published'=>1]);
        }else{
            $request->merge(['published'=>0]);
        }
        $request->merge(['alias'=>Str::slug($request->bienso),'organization_id'=>1]);
        $car = new Car($request->except('_token'));
        $car->save();
        return redirect()->route('car.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        return view('cars.edit',compact('car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car)
    {
        $validatedData = $request->validate([
            'bienso' => ['required'],
            'mucnhienlieu' => ['required'],
        ]);
        if($request->has('published')) {
            $request->merge(['published'=>1]);
        }else{
            $request->merge(['published'=>0]);
        }
        $request->merge(['alias'=>Str::slug($request->bienso),'organization_id'=>1]);
        $car->update($request->except('_token'));
        return redirect()->route('car.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        $car->delete();
        return redirect()->route('car.index')->with('info',"Xóa xong!");
    }
}
