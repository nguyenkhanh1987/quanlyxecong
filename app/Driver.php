<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $fillable = ['name','banglai','ngaycapbang','thoihangbanglai','organization_id','published'];
}
