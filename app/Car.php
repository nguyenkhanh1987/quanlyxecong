<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = ['name','alias','bienso','loainhienlieu','mucnhienlieu','dangki','dangkiem','kmhientai','namsx','published','organization_id'];
}
