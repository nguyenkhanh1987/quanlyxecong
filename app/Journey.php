<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Journey extends Model
{
    protected $fillable = ['name','ngaydi','ngayve','sokm','giaxang','organization_id','published'];
    public function cars() {
        return $this->belongsToMany('App\Car');
    }
}
