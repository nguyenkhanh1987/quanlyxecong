<div id="footer" class="bg-primary text-white py-3 mt-3">
    <div class="container">
        <div class="col text-center">
            <h6>&copy; Bản quyền thuộc về Đại học Thái Nguyên.</h6>
            <h6>Được phát triển bởi <a class="text-white" href="mailto:nguyenkhanh87@gmail.com">Nguyễn Hữu Khánh</a></h6>
        </div>
    </div>
</div>