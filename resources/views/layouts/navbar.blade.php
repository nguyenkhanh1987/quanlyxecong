<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">
    <a class="navbar-brand" href="./">
      <span class="sitename">Quản lý xe công</span>
      <span class="slogan">Chính xác, hiệu quả, tiết kiệm</span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item {{ request()->routeIs('about')?'active':'' }}">
          <a class="nav-link" href="./">{{ __("home.about") }} <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item dropdown {{ request()->routeIs('journey.*')?'active':'' }}">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ __("home.journey_manage") }}
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item {{ request()->routeIs('journey.index')?'active':'' }}" href="{{ route('journey.index') }}">{{ __("home.journey_list") }}</a>
            <a class="dropdown-item {{ request()->routeIs('journey.create')?'active':'' }}" href="{{ route('journey.create') }}">{{ __("home.journey_create") }}</a>
          </div>
        </li>
        <li class="nav-item dropdown {{ request()->routeIs('car.*')?'active':'' }}">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ __("home.car_manage") }}
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item {{ request()->routeIs('car.index')?'active':'' }}" href="{{ route('car.index') }}">{{ __("home.car_list") }}</a>
            <a class="dropdown-item {{ request()->routeIs('car.create')?'active':'' }}" href="{{ route('car.create') }}">{{ __("home.car_create") }}</a>
          </div>
        </li>
        <li class="nav-item dropdown {{ request()->routeIs('driver.*')?'active':'' }}">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ __("home.driver_manage") }}
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item {{ request()->routeIs('driver.index')?'active':'' }}" href="{{ route('driver.index') }}">{{ __("home.driver_list") }}</a>
            <a class="dropdown-item {{ request()->routeIs('driver.create')?'active':'' }}" href="{{ route('driver.create') }}">{{ __("home.driver_create") }}</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>