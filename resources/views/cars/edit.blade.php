@extends('index')

@section('pagetitle', __("home.car_update"))

@push('scripts')
    <script src="{{ asset('js/form_edit.js') }}"></script>
@endpush

@section('content')
    <div class="container">
        <h3 class="my-3">{{ __("home.car_update") }}</h3>
        <form action="{{ route('car.update',$car) }}" class="car-create" method="POST">
            @method('PUT')
            @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="carName">{{ __('home.car_name') }}</label>
                  <input type="text" name="name" value="{{ $car->name }}" class="form-control @error('name') is-invalid @enderror" id="carName">
                </div>
                <div class="form-group col-md-6">
                  <label for="carLicense">{{ __('home.car_license') }}</label>
                  <input type="text" name="bienso" value="{{ $car->bienso }}" class="form-control @error('bienso') is-invalid @enderror" id="carLicense">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="carFuelType">{{ __('home.car_fuel_type') }}</label>
                  <select name="loainhienlieu" class="form-control @error('loainhienlieu') is-invalid @enderror" id="carFuelType">
                      <option @if($car->loainhienlieu==1) {{ "selected" }} @endif value="1">{{ __("home.fuel_type_1") }}</option>
                      <option @if($car->loainhienlieu==2) {{ "selected" }} @endif value="2">{{ __("home.fuel_type_2") }}</option>
                      <option @if($car->loainhienlieu==3) {{ "selected" }} @endif value="3">{{ __("home.fuel_type_3") }}</option>
                  </select>
                </div>
                <div class="form-group col-md-6">
                  <label for="carFuel">{{ __('home.car_fuel') }}</label>
                  <input type="number" name="mucnhienlieu" value="{{ $car->mucnhienlieu }}" class="form-control @error('mucnhienlieu') is-invalid @enderror" id="carFuel">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="carDangki">{{ __('home.car_dangki') }}</label>
                  <input type="date" name="dangki" value="{{ $car->dangki }}" class="form-control @error('dangki') is-invalid @enderror" id="carDangki">
                </div>
                <div class="form-group col-md-6">
                  <label for="carDangkiem">{{ __('home.car_dangkiem') }}</label>
                  <input type="date" name="dangkiem" value="{{ $car->dangkiem }}" class="form-control @error('dangkiem') is-invalid @enderror" id="carDangkiem">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="carkmhientai">{{ __('home.car_kmhientai') }}</label>
                  <input type="number" name="kmhientai" value="{{ $car->kmhientai }}" class="form-control @error('kmhientai') is-invalid @enderror" id="carkmhientai">
                </div>
                <div class="form-group col-md-6">
                  <label for="carNSX">{{ __('home.car_namsx') }}</label>
                  <input type="number" min="1980" max="2099" step="1" name="namsx" value="{{ $car->namsx }}" class="form-control @error('namsx') is-invalid @enderror" id="carNSX">
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="custom-control custom-switch mb-2">
                        <input type="checkbox" name="published" @if($car->published==1) {{ "checked" }} @endif class="custom-control-input" id="carPublished">
                        <label class="custom-control-label" for="carPublished">{{ __("home.published") }}</label>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6 col-md-3 col-lg-2 my-1">
                    <button type="submit" class="btn btn-success btn-block">{{ __('home.save') }}</button>
                </div>
                <div class="col-sm-6 col-md-3 col-lg-2 my-1">
                    <button id="deleteBtn" type="button" class="btn btn-warning btn-block">{{ __('home.delete') }}</button>
                </div>
                <div class="col-sm-6 col-md-3 col-lg-2 my-1">
                    <a class="btn btn-danger btn-block" href="{{ route('car.index') }}">{{ __('home.cancel') }}</a>
                </div>
            </div>
        </form>
        <form id="deleteFrm" method="POST" action="{{ route('car.destroy',$car) }}">
            @method("DELETE")
            @csrf
        </form>
    </div>    
@endsection