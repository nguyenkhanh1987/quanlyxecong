@extends('index')

@section('pagetitle', __("home.driver_create"))

@push('scripts')
    
@endpush

@section('content')
    <div class="container">
        <h3 class="my-3">{{ __("home.driver_create") }}</h3>
        <form action="{{ route('driver.store') }}" class="car-create" method="POST">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="driverName">{{ __('home.driver_name') }}</label>
                  <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="driverName">
                </div>
                <div class="form-group col-md-6">
                  <label for="carLicense">{{ __('home.driver_license') }}</label>
                  <input type="text" name="banglai" class="form-control @error('banglai') is-invalid @enderror" id="carLicense">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="carDangki">{{ __('home.driver_ngaycapbang') }}</label>
                  <input type="date" name="ngaycapbang" class="form-control @error('ngaycapbang') is-invalid @enderror" id="carDangki">
                </div>
                <div class="form-group col-md-6">
                  <label for="carDangkiem">{{ __('home.driver_thoihangbanglai') }}</label>
                  <input type="date" name="thoihangbanglai" class="form-control @error('thoihangbanglai') is-invalid @enderror" id="carDangkiem">
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="custom-control custom-switch mb-2">
                        <input type="checkbox" name="published" checked class="custom-control-input" id="carPublished">
                        <label class="custom-control-label" for="carPublished">{{ __("home.published") }}</label>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6 col-md-3 col-lg-2 my-1">
                    <button type="submit" class="btn btn-success btn-block">{{ __('home.save') }}</button>
                </div>
                <div class="col-sm-6 col-md-3 col-lg-2 my-1">
                    <button type="reset" class="btn btn-warning btn-block">{{ __('home.reset') }}</button>
                </div>
                <div class="col-sm-6 col-md-3 col-lg-2 my-1">
                    <a class="btn btn-danger btn-block" href="{{ route('driver.index') }}">{{ __('home.cancel') }}</a>
                </div>
            </div>
        </form>        
    </div>    
@endsection