@extends('index')

@section('pagetitle', __("home.car_list"))

@push('scripts')

@endpush

@section('content')
    <div class="container">
        <h3 class="my-3 text-center">{{ __("home.car_list") }}</h3>
        <div class="col">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col" class="text-center">{{ __('home.driver_name') }}</th>
                        <th scope="col" class="text-center">{{ __('home.driver_license') }}</th>
                        <th scope="col" class="text-center">{{ __('home.driver_ngaycapbang') }}</th>
                        <th scope="col" class="text-center">{{ __('home.driver_thoihangbanglai') }}</th>
                        <th scope="col" class="text-center">{{ __('home.published') }}</th>
                        <th scope="col" class="text-center">{{ __('home.action') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                            <tr>
                                <td scope="row">{{ $item->id }}</td>
                                <td class="text-center"><a href="{{ route('car.edit',$item) }}">{{ $item->name }}</a></td>
                                <td><a href="{{ route('driver.edit',$item) }}">{{ $item->banglai }}</a></td>
                                <td class="text-center">{{ $item->ngaycapbang }}</td>
                                <td class="text-center">{{ $item->thoihangbanglai }}</td>
                                <td class="text-center">
                                    @if($item->published==1)
                                        <i class="fa text-success fa-check"></i>
                                    @else
                                        <i class="fa text-danger fa-times"></i>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-info" href="{{ route('driver.edit',$item) }}"><i class="fa fa-edit"></i></a>
                                    <form class="d-inline" method="POST" action="{{ route('driver.destroy',$item) }}">
                                        @method("DELETE")
                                        @csrf
                                        <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach    
                    </tbody>    
                </table>
            </div>    
        </div>
        <div class="col text-center">{{ $items->links() }}</div>       
    </div>    
@endsection