@extends('index')

@section('pagetitle', __("home.journey_create"))

@push('scripts')
<script src="{{ asset('js/form_edit.js') }}"></script>
@endpush

@section('content')
    <div class="container">
        <h3 class="my-3 text-center">{{ __("home.journey_create") }}</h3>
        <div class="journey-create">
            <form action="{{ route('journey.store') }}" class="car-create" method="POST">
                @csrf
                <div class="form-row">
                    <div class="form-group col-12">
                      <label for="driverName">{{ __('home.journey_name') }}</label>
                      <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="driverName">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="journeydiemdi">{{ __('home.journey_diemdi') }}</label>
                      <input type="text" name="diemdi" class="form-control @error('diemdi') is-invalid @enderror" id="journeydiemdi">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="journeyCar">{{ __('home.car_name') }}</label>
                        <input type="text" name="car_id" class="form-control @error('car_id') is-invalid @enderror" id="journeyCar">
                    </div>                    
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="journeydiemden">{{ __('home.journey_diemden') }}</label>
                        <input type="text" name="diemden" class="form-control @error('diemden') is-invalid @enderror" id="journeydiemden">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="journeyDriver">{{ __('home.driver_name') }}</label>
                        <input type="text" name="driver_id" class="form-control @error('driver_id') is-invalid @enderror" id="journeyDriver">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="jouneyKm">{{ __('home.journey_sokm') }}</label>
                        <input type="text" disabled value="0" name="sokm" class="form-control @error('sokm') is-invalid @enderror" id="jouneyKm">
                    </div>
                    <div class="col-md-2 form-group"><label for="kocojca">&nbsp;</label><button id="calcDistance" type="button" class="btn btn-block btn-info">{{ __('home.tinhkm') }}</button></div>
                    <div class="form-group col-md-6">
                        <label for="journeyNgaydi">{{ __('home.journey_ngaydi') }}</label>
                        <input type="date" name="ngaydi" class="form-control @error('ngaydi') is-invalid @enderror" id="journeyNgaydi">
                    </div>                    
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="carLicense">{{ __('home.car_fuel') }}</label>
                        <input type="number" name="fuel" disabled class="form-control @error('fuel') is-invalid @enderror" id="carLicense">
                    </div>
                    
                    <div class="form-group col-md-6">
                      <label for="journeyNgayve">{{ __('home.journey_ngayve') }}</label>
                      <input type="date" name="ngayve" class="form-control @error('ngayve') is-invalid @enderror" id="journeyNgayve">
                    </div>
                </div>               

                <div class="row">
                    <div class="col-sm-6 col-md-3 col-lg-2 my-1">
                        <button type="submit" class="btn btn-success btn-block">{{ __('home.save') }}</button>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-2 my-1">
                        <button type="reset" class="btn btn-warning btn-block">{{ __('home.reset') }}</button>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-2 my-1">
                        <a class="btn btn-danger btn-block" href="{{ route('journey.index') }}">{{ __('home.cancel') }}</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
