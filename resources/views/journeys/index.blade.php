@extends('index')

@section('pagetitle', __("home.journey_list"))

@push('scripts')

@endpush

@section('content')
    <div class="container">
        <h3 class="my-3 text-center">{{ __("home.journey_list") }}</h3>
        <div class="col">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col" class="text-center">{{ __('home.journey_name') }}</th>
                        <th scope="col" class="text-center">{{ __('home.journey_ngaydi') }}</th>
                        <th scope="col" class="text-center">{{ __('home.journey_ngayve') }}</th>
                        <th scope="col" class="text-center">{{ __('home.journey_sokm') }}</th>                       
                        <th scope="col" class="text-center">{{ __('home.published') }}</th>
                        <th scope="col" class="text-center">{{ __('home.action') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                            <tr>
                                <td scope="row">{{ $item->id }}</td>
                                <td class="text-center"><a href="{{ route('journey.edit',$item) }}">{{ $item->name }}</a></td>
                                <td><a href="{{ route('journey.edit',$item) }}">{{ $item->ngaydi }}</a></td>
                                <td class="text-center">{{ $item->ngayve }}</td>
                                <td class="text-center">{{ $item->sokm }}</td>                           
                                <td class="text-center">
                                    @if($item->published==1)
                                        <i class="fa text-success fa-check"></i>
                                    @else
                                        <i class="fa text-danger fa-times"></i>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-info" href="{{ route('journey.edit',$item) }}"><i class="fa fa-edit"></i></a>
                                    <form class="d-inline" method="POST" action="{{ route('journey.destroy',$item) }}">
                                        @method("DELETE")
                                        @csrf
                                        <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach    
                    </tbody>    
                </table>
            </div>    
        </div>
        <div class="col text-center">{{ $items->links() }}</div>       
    </div>    
@endsection