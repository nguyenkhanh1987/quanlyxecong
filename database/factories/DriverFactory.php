<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Driver;
use Faker\Generator as Faker;

$factory->define(Driver::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'banglai' => $faker->word(2),
        'ngaycapbang' => $faker->date(),
        'thoihangbanglai' => $faker->date(),
        'organization_id' =>  $faker->randomElement(['1','2','3','4','5']), 
    ];
});
