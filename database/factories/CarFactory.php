<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Car;
use Faker\Generator as Faker;

$factory->define(Car::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'bienso' => $faker->unique()->word(10),
        'alias' => $faker->slug(),
        'loainhienlieu' => $faker->randomElement(['1','2']),
        'mucnhienlieu' => $faker->randomDigit(),
        'organization_id' =>  $faker->randomElement(['1','2','3','4', '5']),        
    ];
});
