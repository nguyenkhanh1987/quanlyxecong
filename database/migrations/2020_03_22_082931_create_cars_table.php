<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("bienso")->unique();
            $table->string("alias")->unique();
            $table->tinyInteger("loainhienlieu");
            $table->double("mucnhienlieu");
            $table->date("dangki")->nullable();
            $table->date("dangkiem")->nullable();
            $table->double("kmhientai")->default(0);
            $table->string("namsx")->nullable();
            $table->unsignedBigInteger('organization_id');
            $table->tinyInteger("published")->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
