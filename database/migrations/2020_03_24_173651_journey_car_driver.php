<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class JourneyCarDriver extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journey_car_driver', function (Blueprint $table) {
            $table->unsignedBigInteger("journey_id");
            $table->unsignedBigInteger("car_id");
            $table->unsignedBigInteger("driver_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journey_car_driver');
    }
}
