<?php

use App\Organization;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([CarSeeder::class,DriverSeeder::class,OrganizationSeeder::class]);
    }
}
